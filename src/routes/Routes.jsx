import React from 'react';
import { BrowserRouter as Router, Routes as Routest, Route } from "react-router-dom";

import { Login } from '../pages/Login';

export const Routes = () => {
    return (
        <div>
            <Router>
                <Routest>
                    <Route exact path="/" component={Login}/>
                </Routest>
            </Router>
        </div>
    );
};

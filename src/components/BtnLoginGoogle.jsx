import React from 'react';
import GoogleLogin from 'react-google-login';

export const BtnLoginGoogle = () => {
    
    const responseGoogle = (response) => {
        console.log(response);
    }

    return (
        <>
            <GoogleLogin
                clientId="658977310896-knrl3gka66fldh83dao2rhgbblmd4un9.apps.googleusercontent.com"
                buttonText="CONTINUAR CON GOOGLE"
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
                cookiePolicy={'single_host_origin'}
                STYLE={'padding-left: 2rem'}

            />
        </>
    );
};

import React from 'react';
import FacebookLogin from 'react-facebook-login';
import { GrFacebook } from "react-icons/gr";

export const BtnLoginFacebook = () => {

    const componentClicked = (response) => {
        console.log(response);               
    }

    const responseFacebook = (response) => {
        console.log(response);               
    }
    return (
        <>
            <FacebookLogin
                appId="612652996670051"
                autoLoad={true}
                fields="name,email,picture"
                onClick={componentClicked}
                callback={responseFacebook} 
                icon= {<GrFacebook size={"1.4em"}/> }
                textButton=" Continuar con Facebook"
                size={'medium'}
                
            />
        </>
    );
};
